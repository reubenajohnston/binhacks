import io
import os
import sys
import argparse

#usage:
# $ python decode.py asteroids <SOURCEDIR> <DESTDIR>
# $ python decode.py asteroids_deluxe <SOURCEDIR> <DESTDIR>
# $ python decode.py gravitar <SOURCEDIR> <DESTDIR>
# from <DESTDIR>
# $ zip asteroid.zip 035127.02 035143.02 035145.02 035144.02
# $ zip astdelux.zip 036430.02 036431.02 036432.02 036433.03 036800.02 036799.01
# $ zip gravitar.zip 136*
ffight_gfx = [(
    [
        ("ff_05.bin", "ff_07.bin", "ff_01.bin", "ff_03.bin")
    ],
    "gfx.bin",
    512 * 1024
)]

# split vrom into two sections 0x0-0x7FFFFF and 0x800000-C00000
# dd if=SuperStreetFighterII.vrom of=SuperStreetFighterII.vrom.1 bs=$((0x800000)) count=1 skip=0
# dd if=SuperStreetFighterII.vrom of=SuperStreetFighterII.vrom.1 bs=$((0x400000)) count=1 skip=2
ssf2_gfx1 = [(
    "SuperStreetFighterII.vrom",
    [
        ("ssf.13m", "ssf.15m", "ssf.17m", "ssf.19m")
    ],
    [
        (0x000000, 0x200000, 0x400000, 0x600000)
    ],
    0x100000
    #first two bytes from 0x000000 offset
    #second two bytes from 0x100000 offset    
)]

ssf2_gfx2 = [(
    "SuperStreetFighterII.vrom",
    [
        ("ssf.14m", "ssf.16m","ssf.18m","ssf.20m")
    ],
    [
        (0x800000, 0x900000, 0xA00000, 0xB00000)
    ],
    0x100000    
    #first two bytes from 0x000000 offset
    #second two bytes from 0x100000 offset
)]

ssf2tu_gfx1 = [(
    "SuperStreetFighterIITurbo.vrom",
    [
        ("sfx.13m", "sfx.15m", "sfx.17m", "sfx.19m")
    ],
    [
        (0x000000, 0x200000, 0x400000, 0x600000)
    ],
    0x100000
    #first two bytes from 0x000000 offset
    #second two bytes from 0x100000 offset    
)]

ssf2tu_gfx2 = [(
    "SuperStreetFighterIITurbo.vrom",
    [
        ("sfx.14m", "sfx.16m","sfx.18m","sfx.20m")
    ],
    [
        (0x800000, 0x900000, 0xA00000, 0xB00000)
    ],
    0x100000    
    #first two bytes from 0x000000 offset
    #second two bytes from 0x100000 offset
)]

ssf2tu_gfx3 = [(
    "SuperStreetFighterIITurbo.vrom",
    [
        ("sfx.21m", "sfx.23m","sfx.25m","sfx.27m")
    ],
    [
        (0xC00000, 0xD00000, 0xE00000, 0xF00000)
    ],
    0x100000    
    #first two bytes from 0x000000 offset
    #second two bytes from 0x100000 offset
)]

sfau_gfx1 = [(
    "StreetFighterAlpha.vrom",
    [
        ("sfz.14m", "sfz.16m", "sfz.18m", "sfz.20m")
    ],
    [
        (0x000000, 0x200000, 0x400000, 0x600000)
    ],
    0x100000
)]

sfa2ur1_gfx1 = [(
    "StreetFighterAlpha2.vrom",
    [
        ("sz2.13m", "sz2.15m", "sz2.17m", "sz2.19m")
    ],
    [
        (0x000000, 0x400000, 0x800000, 0xC00000)
    ],
    0x040000
    #first two bytes from 0x000000 offset
    #second two bytes from 0x040000*4 offset   
)]

sfa2ur1_gfx2 = [(
    "StreetFighterAlpha2.vrom",
    [
        ("sz2.14m", "sz2.16m","sz2.18m","sz2.20m")
    ],
    [
        (0x1000000, 0x1100000, 0x1200000, 0x1300000)
    ],
    0x100000    
)]

asteroids = [(
    ["035145.02", "035144.02", "035143.02", "035127.02"],
    "Asteroids.rom",
    [0x800, 0x800, 0x800, 0x800]
)]

asteroids_deluxe = [(
    ["036430.02", "036431.02", "036432.02", "036433.03", "036800.02", "036799.01"],
    "AsteroidsDeluxe.rom",
    [0x800, 0x800, 0x800, 0x800, 0x800, 0x800]
)]

gravitar = [(
    ["136010-301.d1", "136010-302.ef1", "136010-303.h1", "136010-304.j1", "136010-305.kl1", "136010-306.m1", "136010-210.l7", "136010-207.mn7", "136010-208.np7", "136010-309.r7"],
    "Gravitar.rom",
    [0x1000, 0x1000, 0x1000, 0x1000, 0x1000, 0x1000, 0x800, 0x1000, 0x1000, 0x1000]
)]

#bank size is 0x80000

def usage():
    print("Usage : %s src_game_name rootdir dstdir" % sys.argv[0])
    sys.exit(0)

#https://github.com/fesh0r/old-mame/blob/master/src/mame/video/cps1.c#L1720
def decode_cps1_gfx(data):
    buf = bytearray(data)
    for i in range(0, len(buf), 4):
        dwval = 0
        src = buf[i] + (buf[i + 1] << 8) + (buf[i + 2] << 16) + (buf[i + 3] << 24)

        for j in range(8):
            n = src >> (j * 4) & 0x0f
            if (n & 0x01):
                dwval |= 1 << (     7 - j)
            if (n & 0x02):
                dwval |= 1 << ( 8 + 7 - j)
            if (n & 0x04):
                dwval |= 1 << (16 + 7 - j)
            if (n & 0x08):
                dwval |= 1 << (24 + 7 - j)

        buf[i + 0] = (dwval)       & 0xff
        buf[i + 1] = (dwval >>  8) & 0xff
        buf[i + 2] = (dwval >> 16) & 0xff
        buf[i + 3] = (dwval >> 24) & 0xff
    return buf
    
def split_gfx_file(root_dir, dst_dir, ffight_gfx):  
    for (dst_names, src_file, size) in ffight_gfx:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)          
        with open(src_path, "rb") as src:
            print(src_path)                  
            for (dst_name_1, dst_name_2, dst_name_3, dst_name_4) in dst_names:
                print("\t" + dst_name_1 + ", " + dst_name_2 + ", " + dst_name_3 + ", " + dst_name_4)
                dst_path_1 = os.path.join(dst_dir, dst_name_1)
                dst_path_2 = os.path.join(dst_dir, dst_name_2)
                dst_path_3 = os.path.join(dst_dir, dst_name_3)
                dst_path_4 = os.path.join(dst_dir, dst_name_4)
                with open(dst_path_1, "wb") as dst_1:
                    with open(dst_path_2, "wb") as dst_2:
                        with open(dst_path_3, "wb") as dst_3:
                            with open(dst_path_4, "wb") as dst_4:
                                for i in range(0,size*4,8):
                                    data = decode_cps1_gfx(src.read(8))
                                    dst_1.write(data[0:2])
                                    dst_2.write(data[2:4])
                                    dst_3.write(data[4:6])
                                    dst_4.write(data[6:8])    

def split_gfx_file_option1(root_dir, dst_dir, gfx):  
    for (src_file, dst_names, offsets, size) in gfx:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)
        dst_path1 = os.path.join(dst_dir, dst_names[0][0])    
        dst_path2 = os.path.join(dst_dir, dst_names[0][1])    
        dst_path3 = os.path.join(dst_dir, dst_names[0][2])    
        dst_path4 = os.path.join(dst_dir, dst_names[0][3])    
        with open(src_path, "rb") as src:
            with open(dst_path1, "wb") as dst1:
                with open(dst_path2, "wb") as dst2:
                    with open(dst_path3, "wb") as dst3:
                        with open(dst_path4, "wb") as dst4:
                            for i in range(len(dst_names[0])):
                                offset=offsets[0][i]
                                src.seek(offset,0)
                                data1=src.read(int(size))
                                data2=src.read(int(size))
                                for j in range(0,len(data1)+1,8):
                                    temp1=data1[j:j+8]
                                    temp1_decode=decode_cps1_gfx(temp1)
                                    temp2=data2[j:j+8]
                                    temp2_decode=decode_cps1_gfx(temp2)
                                    dst1.write(temp1_decode[0:2])
                                    dst1.write(temp2_decode[0:2])
                                    dst2.write(temp1_decode[2:4])
                                    dst2.write(temp2_decode[2:4])                    
                                    dst3.write(temp1_decode[4:6])
                                    dst3.write(temp2_decode[4:6]) 
                                    dst4.write(temp1_decode[6:8])
                                    dst4.write(temp2_decode[6:8])      

def split_gfx_file_option2(root_dir, dst_dir, gfx):  
    for (src_file, dst_names, offsets, size) in gfx:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)
        dst_path1 = os.path.join(dst_dir, dst_names[0][0])    
        dst_path2 = os.path.join(dst_dir, dst_names[0][1])    
        dst_path3 = os.path.join(dst_dir, dst_names[0][2])    
        dst_path4 = os.path.join(dst_dir, dst_names[0][3])    
        with open(src_path, "rb") as src:
            with open(dst_path1, "wb") as dst1:
                with open(dst_path2, "wb") as dst2:
                    with open(dst_path3, "wb") as dst3:
                        with open(dst_path4, "wb") as dst4:
                            offset=offsets[0][0]
                            src.seek(offset,0)
                            data1=src.read(int(size))
                            offset=offsets[0][1]
                            src.seek(offset,0)                                
                            data2=src.read(int(size))
                            for j in range(0,len(data1)+1,8):
                                temp1=data1[j:j+8]
                                temp1_decode=decode_cps1_gfx(temp1)
                                temp2=data2[j:j+8]
                                temp2_decode=decode_cps1_gfx(temp2)
                                dst1.write(temp1_decode[0:2])
                                dst1.write(temp2_decode[0:2])
                                dst2.write(temp1_decode[2:4])
                                dst2.write(temp2_decode[2:4])                    
                                dst3.write(temp1_decode[4:6])
                                dst3.write(temp2_decode[4:6]) 
                                dst4.write(temp1_decode[6:8])
                                dst4.write(temp2_decode[6:8])   
                            offset=offsets[0][2]
                            src.seek(offset,0)
                            data1=src.read(int(size))
                            offset=offsets[0][3]
                            src.seek(offset,0)                                
                            data2=src.read(int(size))
                            for j in range(0,len(data1)+1,8):
                                temp1=data1[j:j+8]
                                temp1_decode=decode_cps1_gfx(temp1)
                                temp2=data2[j:j+8]
                                temp2_decode=decode_cps1_gfx(temp2)
                                dst1.write(temp1_decode[0:2])
                                dst1.write(temp2_decode[0:2])
                                dst2.write(temp1_decode[2:4])
                                dst2.write(temp2_decode[2:4])                    
                                dst3.write(temp1_decode[4:6])
                                dst3.write(temp2_decode[4:6]) 
                                dst4.write(temp1_decode[6:8])
                                dst4.write(temp2_decode[6:8])                                    

def split_gfx_file_option3(root_dir, dst_dir, gfx):  
    for (src_file, dst_names, offsets, size) in gfx:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)
        dst_path1 = os.path.join(dst_dir, dst_names[0][0])    
        dst_path2 = os.path.join(dst_dir, dst_names[0][1])       
        dst_path3 = os.path.join(dst_dir, dst_names[0][2])       
        dst_path4 = os.path.join(dst_dir, dst_names[0][3])       
        with open(src_path, "rb") as src:
            with open(dst_path1, "wb") as dst1:
                with open(dst_path2, "wb") as dst2:
                    with open(dst_path3, "wb") as dst3:
                        with open(dst_path4, "wb") as dst4:
                            for i in range(0,7+1,1):
                                data1=src.read(int(size*4))
                                data2=src.read(int(size*4))
                                for i in range(0,int(size*8)+1,8):
                                    temp1=data1[i:i+8]
                                    temp1_decode=decode_cps1_gfx(temp1)
                                    temp2=data2[i:i+8]
                                    temp2_decode=decode_cps1_gfx(temp2)                                
                                    dst1.write(temp1_decode[0:2])
                                    dst1.write(temp2_decode[0:2])
                                    dst2.write(temp1_decode[2:4])
                                    dst2.write(temp2_decode[2:4])                    
                                    dst3.write(temp1_decode[4:6])
                                    dst3.write(temp2_decode[4:6]) 
                                    dst4.write(temp1_decode[6:8])
                                    dst4.write(temp2_decode[6:8])  
                                           
def split_file_option1(root_dir, dst_dir, dest_files):  
    for (dst_names, src_file, sizes) in dest_files:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)          
        with open(src_path, "rb") as src:
            for i in range(len(dst_names[0])):
                dst_name=dst_names[0][i]
                print(dst_name)
                dst_path = os.path.join(dst_dir, dst_name)
                size=sizes[i]
                print(size)
                with open(dst_path, "wb") as dst:
                    for i in range(0,size,8):
                        data = src.read(8)
                        dst.write(data)        
                        
def split_file_option2(root_dir, dst_dir, dest_files):  
    for (dst_names, src_file, sizes) in dest_files:
        src_path=os.path.join(root_dir + "/" + src_file)
        print(src_path)          
        with open(src_path, "rb") as src:
            for i in range(len(dst_names[0])):
                dst_name=dst_names[0][i]
                print(dst_name)
                dst_path = os.path.join(dst_dir, dst_name)
                size=sizes[i]
                print(size)
                with open(dst_path, "wb") as dst:
                    for i in range(0,size,8):
                        data = decode_cps1_gfx(src.read(8))
                        dst.write(data)                

def read_and_decode_gfx(root_dir, dst_dir, src_file, dst_file, size):  
    src_path=os.path.join(root_dir + "/" + src_file)
    print(src_path)          
    with open(src_path, "rb") as src:
        print(dst_file)
        dst_path = os.path.join(dst_dir, dst_file)
        print(size)
        with open(dst_path, "wb") as dst:
            for i in range(0,size,8):
                temp=src.read(8)
                data = decode_cps1_gfx(temp)
                #print(temp.hex()+","+data.hex())
                dst.write(data)                
                                                                        
def main(argc, argv):
    if argc != 4:
        usage()

    parser = argparse.ArgumentParser()
    parser.add_argument("src_game_name", help="source game name",
                        type=str)
    parser.add_argument("rootdir", help="filename for directory with original binary",
                        type=str)
    parser.add_argument("dstdir", help="destination directory",
                        type=str)
    
    args = parser.parse_args()        

    print(args.src_game_name+"\n")

    if args.src_game_name=="ffight_gfx":
        split_gfx_file(args.rootdir, args.dstdir, ffight_gfx)
    elif args.src_game_name=="asteroids":
        split_file_option1(args.rootdir, args.dstdir, asteroids)
    elif args.src_game_name=="asteroids_deluxe":
        split_file_option1(args.rootdir, args.dstdir, asteroids_deluxe)
    elif args.src_game_name=="gravitar":
        split_file_option1(args.rootdir, args.dstdir, gravitar)
    elif args.src_game_name=="ssf2_gfx1":
        split_gfx_file_option1(args.rootdir, args.dstdir, ssf2_gfx1)    
    elif args.src_game_name=="ssf2_gfx2":
        split_gfx_file_option2(args.rootdir, args.dstdir, ssf2_gfx2)   
    elif args.src_game_name=="ssf2tu_gfx1":
        split_gfx_file_option1(args.rootdir, args.dstdir, ssf2tu_gfx1)    
    elif args.src_game_name=="ssf2tu_gfx2":
        split_gfx_file_option2(args.rootdir, args.dstdir, ssf2tu_gfx2)              
    elif args.src_game_name=="ssf2tu_gfx3":
        split_gfx_file_option2(args.rootdir, args.dstdir, ssf2tu_gfx3)    
    elif args.src_game_name=="sfau_gfx1":
        split_gfx_file_option1(args.rootdir, args.dstdir, sfau_gfx1)       
    elif args.src_game_name=="sfa2ur1_gfx1":
        split_gfx_file_option3(args.rootdir, args.dstdir, sfa2ur1_gfx1)       
    elif args.src_game_name=="sfa2ur1_gfx2":
        split_gfx_file_option2(args.rootdir, args.dstdir, sfa2ur1_gfx2)                           
    elif args.src_game_name=="ssf2_vrom":  
        read_and_decode_gfx(args.rootdir, args.dstdir, "SuperStreetFighterII.vrom", "SuperStreetFighterII.vrom.decoded", 12582912)
    elif args.src_game_name=="ssf2tu_vrom":  
        read_and_decode_gfx(args.rootdir, args.dstdir, "SuperStreetFighterIITurbo.vrom", "SuperStreetFighterIITurbo.vrom.decoded", 16777216)      
    elif args.src_game_name=="sfa2ur1_vrom":  
        read_and_decode_gfx(args.rootdir, args.dstdir, "StreetFighterAlpha2.vrom", "StreetFighterAlpha2.vrom.decoded", 20971520)          
    else:
        print("Unsupported binary.")

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)