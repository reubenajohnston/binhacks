import json,shutil,os,io,sys,argparse

# convert binary needs imagemagick package
# $ sudo apt-get install imagemagick

#possibly, the resolutions are incorrect

#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 1 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_0 --imagesize 4096x2048 --imagedepth 8
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 1 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_1 --imagesize 4096x4096 --imagedepth 8
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 1 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_2 --imagesize 4096x4096 --imagedepth 8  
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 1 --jsonfile title_opening_screen.psb.json --streamfolder title_opening_screen.psb.streams --streamfile stream_0 --imagesize 4096x2048 --imagedepth 8  

#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 2 --jsonfile title_opening_screen.psb.json --streamfolder title_opening_screen.psb.streams --streamfile stream_0 --icon "tex"
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 2 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_0 --icon "tex#002"
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 2 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_2 --icon "tex#000"
#"/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/alldata.psb.m_extracted/020/motion" "/mnt/hgfs/shared/re/konami/Castlevania Anniversary Collection/dumps/images" 2 --jsonfile title_grandmenu.psb.json --streamfolder title_grandmenu.psb.streams --streamfile stream_1 --icon "tex#001"

# sega mini: /home/rjohn177/re/sega/dump/alldata.psb.m_extracted/031/motion/us_titleselect_us.psb.json
# use tex#001 with stream0 and vice versa

def usage():
    print("Usage : %s <JSONDIR> <IMAGEDIR> <ACTIONTYPE> --jsonfile <JSONFILE> --streamfolder <STREAMFOLDER> --streamfile <STREAMFILE> --imagesize <WIDTHxHEIGHT> --imagedepth <DEPTH> --icon <ICON> " % sys.argv[0])
    sys.exit(0)

def extractRawImage(jsondir,streamfolder,streamfile,imagedir,imagesize,imagedepth):
    streamsfullfile=os.path.join(jsondir+"/"+streamfolder+"/"+streamfile)
    dstimagefullfile=os.path.join(imagedir+"/"+streamfolder+"."+streamfile+".png")
    cmdstr="convert -size "+imagesize+" -depth "+str(imagedepth)+" bgra:"+"\""+streamsfullfile+"\""+" \""+dstimagefullfile+"\""
    print(cmdstr)
    os.system(cmdstr)
        
def extractTiledImages(jsondir,jsonfile,streamfolder,streamfile,iconstr,imagedir):
    jsonfullfile=os.path.join(jsondir+"/"+jsonfile)
    frawfile=os.path.join(imagedir+"/"+streamfolder+"."+streamfile+".png")
    with open(jsonfullfile,"r") as read_file:
        jsondata=json.load(read_file)

    tsource=jsondata.get('source')
    tex=tsource.get(iconstr)
    texicons=tex.get('icon')

    #Set rawfile=/home/rjohn177/re/sega/dump/us_titleselect_us.psb.streams.stream_0.data
    #Loop over data.source.tex#000.icon elements
      #get the left,top,width,height values
      #get tempfile name (game.png)
      #copy rawfile to tempfile using $ cp rawfile tempfile
      #crop tempfile using $ mogrify -crop widthxheight+left+top temp.png
    for texiconid,texiconvals in enumerate([texicons]):
        print('Iterate through tex'+str(texiconid))
        for key,value in texiconvals.items():
            icon=key
            left=value.get('left')
            top=value.get('top')
            width=value.get('width')
            height=value.get('height')
            print('\t'+icon+':('+str(left)+','+str(top)+'), ('+str(width)+','+str(height)+')')
            ftarg=os.path.join(imagedir+"/"+streamfolder+"."+streamfile+"."+icon+".png")
            cmdstr="cp \""+frawfile+"\" \""+ftarg+"\""
            os.system(cmdstr)
            os.system('mogrify -crop '+str(width)+'x'+str(height)+'+'+str(left)+'+'+str(top)+' '+"\""+ftarg+"\"")

def main(argc, argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("jsondir", help="e.g., /mnt/hgfs/shared/re/dumps/alldata.psb.m_extracted/020/motion",
                        type=str)
    parser.add_argument("imagedir", help="e.g., /mnt/hgfs/shared/re/dumps/images",
                        type=str)
    parser.add_argument("actiontype", help="1-extractRawImage, 2-extractTiledImages",
                        type=int)
    parser.add_argument("--jsonfile", help="e.g., title_grandmenu.psb.json",
                        type=str)
    parser.add_argument("--streamfolder", help="e.g., title_grandmenu.psb.streams",
                        type=str)
    parser.add_argument("--streamfile", help="e.g., stream_0",
                        type=str)    
    parser.add_argument("--imagesize", help="e.g., 4096x2048",
                        type=str)
    parser.add_argument("--imagedepth", help="e.g., 8",
                        type=int)
    parser.add_argument("--icon", help="e.g., tex#000",
                        type=str)
                    
    #put in a try/catch and call usage on error
    args = parser.parse_args()
    
    if args.actiontype==1:#extractRawImage
        extractRawImage(args.jsondir,args.streamfolder,args.streamfile,args.imagedir,args.imagesize,args.imagedepth)        
    elif args.actiontype==2:#extractTiledImages
        extractTiledImages(args.jsondir,args.jsonfile,args.streamfolder,args.streamfile,args.icon,args.imagedir)
    else:
        print("Unsupported actiontype")

    print("Finished")

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)  

