import io
import sys
import argparse

def usage():
    print("Usage : %s evenfile oddfile interleavedfile" % sys.argv[0])
    sys.exit(0)
    
def main(argc, argv):
    if argc != 4:
        usage()

    parser = argparse.ArgumentParser()
    parser.add_argument("evenfilestr", help="filename for even bytes",
                        type=str)
    parser.add_argument("oddfilestr", help="filename for odd bytes",
                        type=str)
    parser.add_argument("interleavedfilestr", help="filename for interleaved bytes",
                        type=str)
    
    args = parser.parse_args()      

    even = bytearray(io.open( args.evenfilestr, "rb" ).read())
    odd = bytearray(io.open( args.oddfilestr, "rb" ).read())
    
    print(args.evenfilestr+' '+args.oddfilestr+' '+args.interleavedfilestr)
    
    interleaved=bytearray()
    for i in range(0,len(even)):
        interleaved.append(even[i])
        interleaved.append(odd[i])
    
    io.open( args.interleavedfilestr, "wb" ).write( interleaved )

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)